package edu.bbte.idde.example.repo.jdbc;

import edu.bbte.idde.example.repo.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Deque;
import java.util.LinkedList;

public final class ConnectionPool {
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionPool.class);
    private static final Integer POOL_SIZE = 4;

    private static ConnectionPool instance;
    private final Deque<Connection> pool = new LinkedList<>();

    private ConnectionPool() {
        // pool felépítése
        try {
            Class.forName("org.h2.Driver");
            String connectionUrl = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
            for (int i = 0; i < POOL_SIZE; ++i) {
                pool.push(DriverManager.getConnection(connectionUrl));
            }
            LOG.info("Initialized pool of size {}", POOL_SIZE);
        } catch (ClassNotFoundException | SQLException e) {
            LOG.error("Connection could not be established", e);
            throw new RepositoryException("Connection could not be established", e);
        }
    }

    public static synchronized ConnectionPool getInstance() {
        if (instance == null) {
            instance = new ConnectionPool();
        }
        return instance;
    }

    public synchronized Connection getConnection() {
        if (pool.isEmpty()) {
            throw new RepositoryException("No connections in pool");
        }
        LOG.info("Giving out connection from pool");
        return pool.pop();
    }

    public synchronized void returnConnection(Connection connection) {
        if (pool.size() < POOL_SIZE) {
            LOG.info("Returning connection to pool");
            pool.push(connection);
        }
    }
}
