package edu.bbte.idde.lombokexample.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BlogPost extends BaseEntity {
    String title;
    String author;
    String content;
}
