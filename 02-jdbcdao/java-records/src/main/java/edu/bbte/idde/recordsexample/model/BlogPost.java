package edu.bbte.idde.recordsexample.model;

public record BlogPost(
    Long id,
    String title,
    String author,
    String content
) {
}
