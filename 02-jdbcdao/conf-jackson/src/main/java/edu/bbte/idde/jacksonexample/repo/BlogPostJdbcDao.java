package edu.bbte.idde.jacksonexample.repo;

import edu.bbte.idde.jacksonexample.config.ConfigurationFactory;
import edu.bbte.idde.jacksonexample.model.BlogPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BlogPostJdbcDao {
    private static final Logger LOG = LoggerFactory.getLogger(BlogPostJdbcDao.class);
    private final DataSource dataSource;

    public BlogPostJdbcDao(DataSource dataSource) {
        this.dataSource = dataSource;

        // JDBC táblázatkészítés beállítás használata
        var jdbcConfiguration = ConfigurationFactory.getJdbcConfiguration();
        if (jdbcConfiguration.isCreateTables()) {
            try (var connection = dataSource.getConnection();
                 var statement = connection.createStatement()) {
                LOG.info("Creating table for DAO");

                statement.executeUpdate("create table BlogPost ("
                    + "id bigint primary key,"
                    + "author varchar(128),"
                    + "title varchar(128),"
                    + "content varchar(1024))");
                statement.close();
            } catch (SQLException e) {
                LOG.error("Error creating table", e);
            }
        }
    }

    public void create(BlogPost blogPost) {
        String query = "insert into BlogPost values (?, ?, ?, ?)";

        try (var connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, blogPost.getId());
            preparedStatement.setString(2, blogPost.getTitle());
            preparedStatement.setString(3, blogPost.getAuthor());
            preparedStatement.setString(4, blogPost.getContent());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Error creating entity", e);
            throw new RepositoryException("Error creating entity", e);
        }
    }

    public BlogPost findById(Long id) {
        var query = "SELECT id, author, title, content FROM BlogPost WHERE id = ?";

        try (var connection = dataSource.getConnection();
             var preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);

            try (var resultSet = preparedStatement.executeQuery()) {
                if (!resultSet.next()) {
                    return null;
                }
                var blogPost = new BlogPost();
                blogPost.setId(resultSet.getLong(1));
                blogPost.setAuthor(resultSet.getString(2));
                blogPost.setTitle(resultSet.getString(3));
                blogPost.setContent(resultSet.getString(4));
                return blogPost;
            }
        } catch (SQLException e) {
            LOG.error("Error finding entity", e);
            throw new RepositoryException("Error finding entity", e);
        }
    }
}
