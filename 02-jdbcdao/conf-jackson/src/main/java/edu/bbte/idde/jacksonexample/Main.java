package edu.bbte.idde.jacksonexample;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.jacksonexample.config.ConfigurationFactory;
import edu.bbte.idde.jacksonexample.model.BlogPost;
import edu.bbte.idde.jacksonexample.repo.BlogPostJdbcDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        // pool felépítése
        var jdbcConfiguration = ConfigurationFactory.getJdbcConfiguration();
        var connectionPoolConfiguration = ConfigurationFactory.getConnectionPoolConfiguration();

        // adatbázis kapcsolat
        var hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(jdbcConfiguration.getDriverClass());
        hikariDataSource.setJdbcUrl(jdbcConfiguration.getUrl());
        hikariDataSource.setMaximumPoolSize(connectionPoolConfiguration.getPoolSize());
        LOG.info("Initialized pool of size {}", connectionPoolConfiguration.getPoolSize());

        var dao = new BlogPostJdbcDao(hikariDataSource);

        // beszúrunk egy sort
        LOG.info("Creating entity");
        dao.create(new BlogPost(42L, "Title 1", "Author 1", "Content 1"));

        // visszakérjük a sort
        BlogPost blogPost = dao.findById(42L);
        LOG.info("Found blog post {}", blogPost);
    }
}
