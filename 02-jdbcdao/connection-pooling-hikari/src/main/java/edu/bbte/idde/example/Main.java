package edu.bbte.idde.example;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.example.model.BlogPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static HikariDataSource buildDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setJdbcUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setMaximumPoolSize(4);
        return dataSource;
    }

    public static void main(String[] args) throws SQLException {
        // connection pool konfigurálása HikariCP-vel
        // kérünk kapcsolatot a poolból, s használjuk
        try (HikariDataSource dataSource = buildDataSource();
             Connection connection = dataSource.getConnection()) {

            // táblázat létrehozásának futtatása
            LOG.info("Creating table");
            // statement létrehozása
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("create table BlogPost (id bigint primary key,"
                    + "author varchar(128),"
                    + "title varchar(128),"
                    + "content varchar(1024))");
                LOG.info("");
            }

            // beszúrunk egy sort
            String query = "insert into BlogPost values (?, ?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, 42L);
                preparedStatement.setString(2, "Title 1");
                preparedStatement.setString(3, "Author 1");
                preparedStatement.setString(4, "Content 1");
                preparedStatement.executeUpdate();
            }

            // visszakérjük a sort
            query = "SELECT id, author, title, content FROM BlogPost WHERE id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, 42L);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    BlogPost blogPost = null;
                    if (resultSet.next()) {
                        blogPost = new BlogPost();
                        blogPost.setId(resultSet.getLong(1));
                        blogPost.setAuthor(resultSet.getString(2));
                        blogPost.setTitle(resultSet.getString(3));
                        blogPost.setContent(resultSet.getString(4));
                    }
                    LOG.info("Found blog post {}", blogPost);
                }
            }
        }
    }
}
