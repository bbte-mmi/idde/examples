package edu.bbte.idde.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

public class L10nExample {
    private static final Logger LOG = LoggerFactory.getLogger(L10nExample.class);

    public static void displayNumber(Locale currentLocale) {
        Integer intExample = 123456;
        Double doubleExample = 345987.246;

        // locale-specific formatter
        NumberFormat numberFormatter = NumberFormat.getNumberInstance(currentLocale);
        LOG.info("Integer  in locale {} is {}", currentLocale, numberFormatter.format(intExample));
        LOG.info("Double   in locale {} is {}", currentLocale, numberFormatter.format(doubleExample));
    }

    public static void displayCurrency(Locale currentLocale) {
        Double currency = 9876543.21;

        // locale-specific currency formatter
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);
        LOG.info("Currency in locale {} is {}", currentLocale, currencyFormatter.format(currency));
    }

    public static void displayPercent(Locale currentLocale) {
        Double percent = 0.75;

        // locale-specific percent formatter
        NumberFormat percentFormatter = NumberFormat.getPercentInstance(currentLocale);
        LOG.info("Percent  in locale {} is {}", currentLocale, percentFormatter.format(percent));
    }

    public static void displayDatetime(Locale currentLocale) {
        Date dateTime = new Date();

        // locale-specific datetime formatter
        DateFormat datetimeFormatter = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, currentLocale);
        LOG.info("Datetime in locale {} is {}", currentLocale, datetimeFormatter.format(dateTime));
    }

    public static void main(String[] args) {
        Locale[] locales = {
            new Locale("en", "US"),
            new Locale("de", "DE"),
            new Locale("fr", "FR"),
        };

        for (Locale locale : locales) {
            LOG.info("");
            displayNumber(locale);
            displayCurrency(locale);
            displayPercent(locale);
            displayDatetime(locale);
        }
    }
}
