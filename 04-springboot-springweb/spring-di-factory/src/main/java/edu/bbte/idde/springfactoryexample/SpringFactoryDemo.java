package edu.bbte.idde.springfactoryexample;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class SpringFactoryDemo {

    /**
     * Átadjuk a control flow-t a Spring alkalmazásnak.
     * A @SpringBootApplication annotáció miatt a jelen csomagot végigszkenneli,
     * s keresi a bennük rejlő beaneket.
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringFactoryDemo.class, args);
    }
}
