package edu.bbte.idde.springfactoryexample.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.lang.reflect.Field;

/**
 * Konfigurációs bean.
 * Eszerint factory-ként dolgozik nem annotált bean típusoknak.
 */
@Configuration
public class ConfigurationBean {

    /**
     * Factory metódus.
     * Visszatérítési típus alapján kezeli a Spring.
     * Minden injektálási pont külön példányt kap ezzel az implementációval.
     */
    @Bean
    public ServiceBean buildServiceBean() {
        return new ServiceBean();
    }

    /**
     * Külső (nem általunk definiált) objektum factory-ja.
     * Az opcionális injectionPoint paraméterből információt kapunk arról,
     * ahova az injektálás történik.
     */
    @Bean
    @Scope("prototype")
    public Logger buildLogger(InjectionPoint injectionPoint) {
        Field field = injectionPoint.getField();
        Class<?> clazz = null;
        if (field != null) {
            clazz = field.getDeclaringClass();
        }
        return LoggerFactory.getLogger(clazz);
    }
}
