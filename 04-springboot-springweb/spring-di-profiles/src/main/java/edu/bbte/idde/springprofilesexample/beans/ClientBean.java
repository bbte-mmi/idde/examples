package edu.bbte.idde.springprofilesexample.beans;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientBean {
    private static final Logger LOG = LoggerFactory.getLogger(ClientBean.class);

    // profil szerint más példányt kapunk
    @Autowired
    private ServiceInterface service;

    @PostConstruct
    public void postConstruct() {
        LOG.debug("In @PostConstruct of ClientBean");
        service.serviceMethod();
    }
}
