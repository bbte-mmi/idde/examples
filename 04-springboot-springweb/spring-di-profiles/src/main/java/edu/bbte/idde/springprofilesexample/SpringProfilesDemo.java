package edu.bbte.idde.springprofilesexample;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class SpringProfilesDemo {

    public static void main(String[] args) {
        SpringApplication.run(SpringProfilesDemo.class, args);
    }
}
