package edu.bbte.idde.springdiexample.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Jelöljük, hogy ez az osztály injektálható.
 * Ha valamely szülő csomagban indul classpath scanning a @ComponentScan annotációval,
 * ez az osztály meg lesz találva.
 */
@Component
public class ServiceBean implements ServiceInterface {
    private static final Logger LOG = LoggerFactory.getLogger(ServiceBean.class);

    @Override
    public void serviceMethod() {
        LOG.info("I am performing a service");
    }
}
