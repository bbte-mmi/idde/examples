package edu.bbte.idde.springwebexample.dto.incoming;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;

public record BookCreationDto(
    @NotEmpty
    @Size(max = 256)
    String title,

    @NotEmpty
    @Size(max = 256)
    String author,

    @Positive
    Integer releaseYear,

    @Pattern(regexp = "[0-9- ]+", message = "Not a valid ISBN number")
    @Size(min = 10, max = 16)
    String isbn,

    @Size(max = 8192)
    String description
) {
}
