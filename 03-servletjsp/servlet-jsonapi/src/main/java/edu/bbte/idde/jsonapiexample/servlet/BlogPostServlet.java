package edu.bbte.idde.jsonapiexample.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bbte.idde.jsonapiexample.model.BlogPost;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet("/blogPosts")
public class BlogPostServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(BlogPostServlet.class);

    // JSON írást támogató objektum
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        LOG.info("GET /blogPosts");

        // jelezzük a HTTP válaszban, hogy JSON típusú kimenetet küldünk
        resp.setHeader("Content-Type", "application/json");

        // visszatérítendő objektum (data transfer object)
        List<BlogPost> blogPosts = Arrays.asList(
            new BlogPost(42L, "Title 1", "Author 1", "Content 1"),
            new BlogPost(43L, "Title 2", "Author 2", "Content 2"),
            new BlogPost(44L, "Title 3", "Author 3", "Content 3")
        );

        // JSON-ná alakítjuk az objektumot,
        // s egyenesen kiírjuk a Servlet kimenetére
        objectMapper.writeValue(resp.getOutputStream(), blogPosts);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        LOG.info("POST /blogPosts");

        // megpróbáljuk a request body-t JSON-ként értelmezni s a megadott osztály példányára alakítani
        // sok hibakezelés szükséges ennél a lépésnél
        BlogPost blogPost = objectMapper.readValue(req.getInputStream(), BlogPost.class);

        LOG.info("Received blog post: {}", blogPost);
    }
}
