package edu.bbte.idde.example.repo;

import edu.bbte.idde.example.model.BlogPost;

import java.util.Collection;

/**
 * Egy bizonyos entitásra specifikus CRUD műveletek.
 */
public interface BlogPostDao extends Dao<BlogPost> {

    /**
     * Szűrt entitáslista visszatérítése.
     */
    Collection<BlogPost> findByAuthor(String author);

    // további blogposzt-specifikus CRUD műveletek...
}
