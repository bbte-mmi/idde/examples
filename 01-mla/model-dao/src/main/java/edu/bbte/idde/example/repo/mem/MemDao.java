package edu.bbte.idde.example.repo.mem;

import edu.bbte.idde.example.model.BaseEntity;
import edu.bbte.idde.example.repo.Dao;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * CRUD műveletek implementációja egy elérési móddal.
 * Itt, a példa kedvéért: memóriában tárolt entitások.
 */
public abstract class MemDao<T extends BaseEntity> implements Dao<T> {

    protected Map<Long, T> entities = new ConcurrentHashMap<>();

    @Override
    public Collection<T> findAll() {
        return entities.values();
    }

    @Override
    public void create(T entity) {
        entities.put(entity.getId(), entity);
    }

    // további itt megvalósítható CRUD műveletek
}
