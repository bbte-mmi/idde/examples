package edu.bbte.idde.jpqlexample.model;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "db_blogpost")
@NamedQueries({
    @NamedQuery(name = BlogPost.FIND_ALL_QUERY, query = "from BlogPost"),
    @NamedQuery(name = BlogPost.FIND_BY_AUTHOR_QUERY, query = "from BlogPost where author=:author"),
})
public class BlogPost extends BaseEntity {

    // konstansok a named query-k nevével
    public static final String FIND_ALL_QUERY = "BlogPost.findAll";
    public static final String FIND_BY_AUTHOR_QUERY = "BlogPost.findByAuthor";

    @Column(nullable = false)
    private String title;
    private String author;
    @Column(length = 1024)
    private String content;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public BlogPost() {
        super();
    }

    public BlogPost(String title, String author, String content, Date date) {
        super();
        this.title = title;
        this.author = author;
        this.content = content;
        this.date = new Date(date.getTime());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return new Date(date.getTime());
    }

    public void setDate(Date date) {
        this.date = new Date(date.getTime());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BlogPost{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", author='").append(author).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }
}
