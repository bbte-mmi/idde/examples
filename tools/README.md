# Webprog példaprogramokhoz segédkörnyezet

- szükséges Docker

## MySQL

- indítás: `docker compose -f mysql.docker-compose.yaml up --remove-orphans -d`
- leállítás: `docker compose  -f mysql.docker-compose.yaml down --remove-orphans`
- leállítás s adatok törlése: `docker compose  -f mysql.docker-compose.yaml down --remove-orphans --volumes`
- MySQL konzol indítás után: `docker exec -it webprog-mysql mysql -u root -pHnsAdrt0WtynpKJd -Dwebprog`

## Tomcat

- indítás: `docker compose -f tomcat.docker-compose.yaml up --remove-orphans -d`
- leállítás: `docker compose  -f tomcat.docker-compose.yaml down --remove-orphans`
- volume megkeresése: `docker volume inspect tools_iddetomcatwebappsvolume | jq -r '.[0].Mountpoint'`
- 
